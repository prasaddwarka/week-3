package com.example.mongodbexample.mongowithspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongowithspringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongowithspringbootApplication.class, args);
	}

}
