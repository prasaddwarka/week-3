package com.example.mongodbexample.mongowithspringboot.Repo;

import com.example.mongodbexample.mongowithspringboot.models.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StudentRepository extends MongoRepository<Student,Integer>{
}
