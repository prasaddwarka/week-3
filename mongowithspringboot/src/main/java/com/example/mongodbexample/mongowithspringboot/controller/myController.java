package com.example.mongodbexample.mongowithspringboot.controller;

import com.example.mongodbexample.mongowithspringboot.Repo.StudentRepository;
import com.example.mongodbexample.mongowithspringboot.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class myController {

    @Autowired
    private StudentRepository studentRepository;

    @PostMapping("/")
    public ResponseEntity<?> addStudent(@RequestBody Student student){
        Student st = this.studentRepository.save(student);
        return ResponseEntity.ok(st);
    }

    @GetMapping("/")
    public ResponseEntity<?> getStudents(){
        return ResponseEntity.ok(this.studentRepository.findAll());
    }


}
