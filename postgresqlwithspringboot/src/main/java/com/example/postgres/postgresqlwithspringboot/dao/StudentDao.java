package com.example.postgres.postgresqlwithspringboot.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StudentDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void createTable(){
        String query = "CREATE TABLE student(id SERIAL PRIMARY KEY, name varchar(255) NOT NULL,city varchar(255))";
        int result = this.jdbcTemplate.update(query);
        System.out.println("No. of Rows Inserted "+ result);
    }

    public void insertData(String name, String city){
        String q = "insert into student(name,city) values(?,?)";
        int result = this.jdbcTemplate.update(q,name,city);
        System.out.println("No. of Rows Inserted "+ result);
    }

    public void updateCity(int id,String city){
        String w= "update student set city=? where id=?";
        int result = this.jdbcTemplate.update(w,city,id);
        System.out.println(result + " Row successfully updated..");
    }

    public void deleteData(int id){
        String w= "DELETE from student where id=?";
        int result = this.jdbcTemplate.update(w,id);
        System.out.println(result + " Row successfully deleted..");
    }
}
