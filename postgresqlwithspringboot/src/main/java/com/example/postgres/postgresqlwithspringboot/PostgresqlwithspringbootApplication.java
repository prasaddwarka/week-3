package com.example.postgres.postgresqlwithspringboot;

import com.example.postgres.postgresqlwithspringboot.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgresqlwithspringbootApplication implements CommandLineRunner {

	@Autowired
	private StudentDao studentDao;

	public static void main(String[] args) {
		SpringApplication.run(PostgresqlwithspringbootApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//creating table
		//this.studentDao.createTable();

		//inserting data
		//this.studentDao.insertData("dwarka","jaipur");
		//this.studentDao.insertData("Rahul","Lacknow");

		//updating data
		//this.studentDao.updateCity(1,"mumbai");

		//Deleting data
		this.studentDao.deleteData(1);
	}
}
